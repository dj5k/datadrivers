resource "aws_ecs_service" "hello_world" {
  name            = "${var.environment}-${var.appname}_public"
  cluster         = aws_ecs_cluster.world.id
  task_definition = aws_ecs_task_definition.hello_world.arn
  desired_count   = 1

  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 100

  network_configuration {
    subnets         = module.vpc.private_subnets
    security_groups = [aws_security_group.ecs_service.id]
  }

  load_balancer {
    target_group_arn = module.alb.target_group_arns[0]
    container_name   = "hello_world"
    container_port   = 8080
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = 1
  }

  lifecycle {
    ignore_changes = [desired_count] # due to autoscaling; add task_definition in case of external deployment process
  }
}

resource "aws_ecs_task_definition" "hello_world" {
  family                   = "${var.environment}-${var.appname}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256 # lowest possible
  memory                   = 512 # lowest possible
  execution_role_arn       = aws_iam_role.app_role.arn
  container_definitions = jsonencode([
    {
      name      = "hello_world"
      image     = "quay.io/kohlstechnology/blackbox-helloworld-responder:v0.1.15"
      essential = true
      portMappings = [
        {
          containerPort = 8080
        }
      ]
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-region        = "eu-central-1"
          awslogs-group         = "${var.appname}-${var.environment}"
          awslogs-stream-prefix = "world"
        }
      }
    }
  ])
}

resource "aws_iam_role" "app_role" {
  name                = "${var.appname}-${var.environment}"
  managed_policy_arns = [data.aws_iam_policy.ecs_execution_policy.arn]

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ecs-tasks.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

data "aws_iam_policy" "ecs_execution_policy" {
  name = "AmazonECSTaskExecutionRolePolicy"
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "${var.appname}-${var.environment}"
  retention_in_days = 1
}

resource "aws_security_group" "ecs_service" {
  name        = "${var.environment}-${var.appname}-ecs_service"
  description = "Allow traffic from ALB to service"
  vpc_id      = module.vpc.vpc_id
}

# avoid circular dependency with alb security group
resource "aws_security_group_rule" "ecs_service_ingress" {
  security_group_id = aws_security_group.ecs_service.id

  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.public_alb.id
}

resource "aws_security_group_rule" "ecs_service_egress" {
  security_group_id = aws_security_group.ecs_service.id

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]
}
