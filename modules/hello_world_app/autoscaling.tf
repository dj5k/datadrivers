resource "aws_appautoscaling_target" "ecs_service" {
  max_capacity       = var.ecs_service_autoscaling.max_capacity
  min_capacity       = var.ecs_service_autoscaling.min_capacity
  resource_id        = "service/${aws_ecs_cluster.world.name}/${aws_ecs_service.hello_world.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_service_rpm" {
  name               = "${var.environment}-${var.appname}-rpm-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_service.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_service.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_service.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${module.alb.lb_arn_suffix}/${module.alb.target_group_arn_suffixes[0]}"
    }

    target_value = var.ecs_service_autoscaling.rpm_target_value
  }
}
