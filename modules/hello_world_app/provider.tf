terraform {
  required_version = "~> 1.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }

  }
}

provider "cloudflare" {}

provider "aws" {
  region = "eu-central-1"
  default_tags {
    tags = {
      environment = var.environment
      billing     = "datadrivers"
    }
  }
}

