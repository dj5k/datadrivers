variable "appname" {
  type    = string
  default = "hello-world"
}

variable "environment" {
  type    = string
  default = "test"
}

variable "ecs_service_autoscaling" {
  type = map(string)
  default = {
    min_capacity     = 2
    max_capacity     = 4
    rpm_target_value = 60
  }

}
