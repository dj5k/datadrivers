resource "aws_ecs_cluster" "world" {
  name = "${var.environment}-world"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "world" {
  cluster_name = aws_ecs_cluster.world.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]
}
