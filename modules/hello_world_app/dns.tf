resource "cloudflare_record" "ecs_service" {
  zone_id = data.cloudflare_zone.this.zone_id
  name    = local.domain_name
  value   = module.alb.lb_dns_name
  type    = "CNAME"
  ttl     = 600
}
