# Infrastructure repository for hello world app on ECS Fargate

This repo contains the code to roll out infrastructure for a hello world app on AWS using terragrunt and terraform.
