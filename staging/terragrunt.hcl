include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_path_to_repo_root()}//modules/hello_world_app"
}

inputs = {
  environment = "staging"
  ecs_service_autoscaling = {
    min_capacity     = 2
    max_capacity     = 10
    rpm_target_value = 60
  }
}