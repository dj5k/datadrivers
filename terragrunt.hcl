remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "tfstate20220503181802742400000001"
    key = "${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    kms_key_id     = "f4d55505-8963-4c1f-99f6-22998b0ee2f1"
    dynamodb_table = "tfstate-lock"
  }
}
